import Vue from 'vue'
import Router from 'vue-router'
import LogIn from '@/components/LogIn'
import Home from '@/components/Home'
import store from '@/store'
import Users from '@/components/users/Users'
import Roles from '@/components/rights/Roles'
import Goods from '@/components/goods/Goods'
import AddTheGoods from '@/components/goods/AddTheGoods'
import Rights from '@/components/rights/Rights'
import Params from '@/components/goods/Params'
const originalPush = Router.prototype.push
Router.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}


Vue.use(Router)


const routes = new Router({
  routes: [
    {
      path: '/',
      redirect: '/login'
    },
    {
      path: '/login',
      name: 'login',
      component: LogIn,
      meta: { requiresAuth: true }
    },
    {
      path: '/home',
      name: 'home',
      component: Home,
      children: [
        {
          path: 'users',
          name: 'users',
          component: Users,
        },
        {
          path: 'roles',
          name: 'roles',
          component: Roles
        },
        {
          path: 'goods',
          name: 'goods',
          component: Goods
        }
        ,
        {
          path: 'goodsadd',
          name: 'goodsadd',
          component: AddTheGoods
        }
        ,
        {
          path:'rights',
          name:'rights',
          component:Rights
        }
        ,
        {
          path:'params',
          name:'params',
          component:Params
        }
      ]
    }
  ],
 
})


routes.beforeEach((to, from, next) => {
  if(!to.meta.requiresAuth) {
    if(store.state.token){
      next()
    }else {
      next({
        path: '/login'
      })
    }
  }else {
    next()
  }
})


export default routes