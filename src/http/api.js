import http from './index'
import axios from 'axios'
// function logIn(data) {
//     return http('login', data, 'post')
// }

// function getMenus() {
//     return http('menus')
// }


// export default {
//     logIn,
//     getMenus
// }


export const logIn = function(data) {
    return http('login', data, 'post')
}

export const getMenus = function() {
    return http('menus')
}

export const getUsers = function(params) {
    return http('users',{} ,'get' ,params)
}

export const addUsers = function(data) {
    return http('users', data, 'POST')
}

export const changeUserStatus = function(data) {
    return http(`users/${data.id}/state/${data.mg_state}`, {}, 'PUT')
}

export const editUsers = function(data) {
    return http(`users/${data.id}`, { email: data.email,mobile: data.mobile}, 'PUT')
}

export const deleteUsers = function(data) {
    return http(`users/${data.id}`, {}, 'DELETE')
}

export const getUserInfoById = function(data) {
    return http(`users/${data.id}`, {}, 'GET')
}

export const getRolesList = function() {
    return http('roles', {}, 'GET')
}

export const editRole = function(data) {
    return http(`users/${data.id}/role`, {
        rid: data.rid
    }, 'PUT')
}

export const deleteRights=function(roleId,rightId){
    return http(`roles/${roleId,roleId}roleId/rights/${rightId,rightId}`,{},'delete')

}

//添加角色
export const addRoles=function(data){
    return http('roles',data,'POST')
}


//获取权限
export const gain=function(val){
   return http(`rights/${!val ? "tree" : "list"}`)
}

//修改权限
export const editRights=function(roleId,rids){
    return http(`roles/${roleId}/rights`,{
rids
    },'post')
 }

 //获取商品列表
 export const getGoodsLists=function(data){
     return http('goods',{},'GET', data)
 }

 //获取商品分类列表
 export const commodityClassifyLists=function(){
     return http('categories?type=3')
 }

 //获取动态参数
 export const getDynamicParameter=function(id){
     return http(`categories/${id}/attributes?sel=many`,{},'GET')
 }

 //获取静态度参数
 export const getOnlyParameter=function(id){
    return http(`categories/${id}/attributes?sel=only`,{},'GET')
}

export const uploadImg=axios.defaults.baseURL+'upload'


//发布商品
export const publicGoods=function(data){
    return http('goods',data,'POST')
}