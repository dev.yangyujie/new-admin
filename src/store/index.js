import Vue from 'vue'
import vuex from 'vuex'

Vue.use(vuex)

export default new vuex.Store({
    state:{
        token:localStorage.getItem('token')||''
    },
    mutations:{
        setToken(state,data){
            state.token=data
            localStorage.setItem('token',data)
        }
    }
})