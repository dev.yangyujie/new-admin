import moment from 'moment'

const dealCreateDate=(val)=>{
    return moment(val).format("YYYY-MM-DD");
}

const dealArr=(val)=>{
return val.split(',')
}

export default {
    dealCreateDate,
    dealArr
}